// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:session0/domain/LogInPresenter.dart';
import 'package:session0/domain/utils.dart';

import 'package:session0/main.dart';
import 'package:session0/presentation/pages/LogIn.dart';
import 'package:session0/presentation/widgets/dialogs.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

void main() {
  group('tests', () {
    test('email valid', (){
      var emailTrue = 'test@test.test';
      bool isValid = checkEmail(emailTrue);
      expect(isValid, true);
      var emailFalse = 'Test@.t';
      bool isValid1 = checkEmail(emailFalse);
      expect(isValid1, false);
    });
  });
  test('password valid', (){
    var password = '';
    bool isValid = validPassword(password);
    expect(isValid, false);
    var passwordTrue = '12345';
    bool isValid1 = validPassword(passwordTrue);
    expect(isValid1, true);

  });

  testWidgets('show error', (tester) async {
    await Supabase.initialize(
      url: 'https://ndpirkfhcctoltoveofs.supabase.co',
      anonKey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Im5kcGlya2ZoY2N0b2x0b3Zlb2ZzIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTM3MTYzMDcsImV4cCI6MjAyOTI5MjMwN30.AUff60YmRHhuygJXd0vUJGWpwQafI-_2JT0pGYM50jI',
    );
    await tester.runAsync(()async
    {tester.pumpWidget(MyApp());});
    await tester.tap(find.byKey(Key('key-button')));
    await tester.pump();
    expect(find.widgetWithText(AlertDialog, 'Error'), findsOneWidget);
  });
}
