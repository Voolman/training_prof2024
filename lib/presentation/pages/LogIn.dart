import 'package:flutter/material.dart';
import 'package:session0/domain/LogInPresenter.dart';
import 'package:session0/domain/utils.dart';
import 'package:session0/presentation/pages/Holder.dart';
import 'package:session0/presentation/widgets/custom_textfield.dart';
import 'package:session0/presentation/widgets/dialogs.dart';

class LogIn extends StatefulWidget {
  const LogIn({super.key});

  @override
  State<LogIn> createState() => _LogInState();
}

class _LogInState extends State<LogIn> {
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  bool enableButton = false;
  void onChanged(_){
    enableButton = checkEmail(email.text) && validPassword(password.text);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          CustomField(label: '', hint: '', controller: email, onChange: onChanged,),
          SizedBox(height: 10),
          CustomField(label: '', hint: '', controller: password, onChange: onChanged,),
          SizedBox(height: 10),
          FilledButton(
            key: Key('key-button'),
              onPressed: (){
                pressLogIn(
                    email.text,
                    password.text,
                    (){
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => Holder()));
                    },
                    (String e){showError(context, e);}
                );
              },
              child: Text('ok'))
        ],
      ),
    );
  }
}