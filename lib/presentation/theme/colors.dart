import 'package:flutter/material.dart';

abstract class ColorsApp{
  abstract final Color blue;
  abstract final Color white;
  abstract final Color black;
  abstract final Color gray;
  abstract final Color lightGray;
  abstract final Color darkGray;
}

class LightColorsApp extends ColorsApp{
  @override
  // TODO: implement black
  Color get black => Color.fromARGB(0, 00, 0, 0);

  @override
  // TODO: implement blue
  Color get blue => Color.fromARGB(255, 72, 178, 231);

  @override
  // TODO: implement darkGray
  Color get darkGray => Color.fromARGB(255, 106, 106, 106);

  @override
  // TODO: implement gray
  Color get gray => Color.fromARGB(255, 112, 123, 129);

  @override
  // TODO: implement lightGray
  Color get lightGray => Color.fromARGB(255, 247, 247, 249);

  @override
  // TODO: implement white
  Color get white => Color.fromARGB(255, 255, 255, 255);

}