import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:session0/presentation/theme/colors.dart';

class CustomCard extends StatefulWidget {
  const CustomCard({super.key});

  @override
  State<CustomCard> createState() => _CustomCardState();
}

class _CustomCardState extends State<CustomCard> {
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Container(
      width: 160.w,
      height: 184.w,
      decoration: BoxDecoration(
        color: colors.white,
        borderRadius: BorderRadius.circular(16)
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              Padding(
                  padding: EdgeInsets.only(top: 3.w, left: 9.w),
                child: Container(
                  height: 28.w,
                  width: 28.w,
                  decoration: BoxDecoration(
                    color: colors.lightGray,
                    borderRadius: BorderRadius.circular(360)
                  ),
                  child: SvgPicture.asset('assets/heart/svg', color: colors.gray, width: 16, height: 16),
                ),
              ),
              Padding(
                  padding: EdgeInsets.only(top: 34, left: 21),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    InkWell(
                      child: Image.asset('assets/cros.png', width: double.infinity, fit: BoxFit.cover,)
                    )
                  ],
                ),
              )
            ],
          ),
          Padding(
              padding: EdgeInsets.only(left: 9.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                SizedBox(height: 12.6.w),
                Text(
                    'Best Seller',
                  style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                      color: colors.blue,
                      fontWeight: FontWeight.w500,
                      fontSize: 12.sp,
                      height: 4/3.w
                    )
                  ),
                ),
                SizedBox(height: 8.w),
                Text(
                  'Nike Air Max',
                  maxLines: 1,
                  style: GoogleFonts.raleway(
                      textStyle: TextStyle(
                          color: colors.darkGray,
                          fontWeight: FontWeight.w600,
                          fontSize: 16.sp,
                          height: 5/4.w
                      )
                  ),
                ),
                Padding(
                    padding: EdgeInsets.only(top: 4.w),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Stack(
                        children: [
                          Padding(
                              padding: EdgeInsets.only(left: 117.w),
                            child: GestureDetector(
                              child: Container(
                                width: 34.w,
                                height: 35.5.w,
                                decoration: BoxDecoration(
                                  color: colors.blue,
                                  borderRadius: BorderRadius.only(topLeft: Radius.circular(16), bottomRight: Radius.circular(16))
                                ),
                                child: Text('+'),
                              ),
                            ),
                          ),
                          Container(
                            height: 35.5.w,
                            alignment: Alignment.center,
                            child: Text(
                              '₽752.00',
                              style: GoogleFonts.poppins(
                                  textStyle: TextStyle(
                                      color: colors.black,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14.sp,
                                      height: 8/7.w
                                  )
                              ),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          )

        ],
      ),
    );
  }
}