import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:session0/presentation/theme/colors.dart';

class CustomBar extends StatefulWidget {
  final Function(int) onSelectTap;
  final Function() onTapCard;
  const CustomBar({super.key, required this.onSelectTap, required this.onTapCard});

  @override
  State<CustomBar> createState() => _CustomBarState();
}

class _CustomBarState extends State<CustomBar> {
  int currentIndex = 0;
  void onTap(int newIndex){
    setState(() {
      currentIndex = newIndex;
    });
    widget.onSelectTap;
  }
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return SizedBox(
      height: 106.w,
      width: double.infinity,
      child: Stack(
        children: [
          Transform.translate(
            offset: Offset(0, -1.5.w),
            child: ImageFiltered(
              imageFilter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: SizedBox(
                width: double.infinity,
                child: SvgPicture.asset('assets/background.svg', fit: BoxFit.fill, color: Color.fromARGB(
                    31, 131, 170, 209),),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(0, 4.w),
            child: ImageFiltered(
              imageFilter: ImageFilter.blur(sigmaX: 15, sigmaY: 15),
              child: SizedBox(
                width: double.infinity,
                child: SvgPicture.asset('assets/background.svg', fit: BoxFit.fill, color: Color.fromARGB(
                    38, 0, 0, 0),),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: SizedBox(
              width: double.infinity,
              height: 106.w,
              child: SvgPicture.asset('assets/background.svg'),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
                padding: EdgeInsets.only(bottom: 30.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    children: [
                      GestureDetector(
                        onTap: () => onTap(0),
                        child: SvgPicture.asset('assets/home.svg', color: (currentIndex == 0) ? colors.blue : colors.gray, width: 24.w, height: 24.w,),
                      ),
                      GestureDetector(
                        onTap: () => onTap(1),
                        child: SvgPicture.asset('assets/heart.svg', color: (currentIndex == 1) ? colors.blue : colors.gray, width: 24.w, height: 24.w,),
                      ),
                    ],
                  ),
                  SizedBox(width: 138.w),
                  Row(
                    children: [
                      GestureDetector(
                        onTap: () => onTap(2),
                        child: SvgPicture.asset('assets/notification.svg', color: (currentIndex == 2) ? colors.blue : colors.gray, width: 24.w, height: 24.w,),
                      ),
                      GestureDetector(
                        onTap: () => onTap(3),
                        child: SvgPicture.asset('assets/profile.svg', color: (currentIndex == 3) ? colors.blue : colors.gray, width: 24.w, height: 24.w,),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
                padding: EdgeInsets.only(bottom: 50.w),
              child: GestureDetector(
                onTap: widget.onTapCard,
                  child: Container(
                    height: 56.w,
                    width: 56.w,
                    decoration: BoxDecoration(
                      color: colors.blue,
                      borderRadius: BorderRadius.circular(360),
                      boxShadow: [
                        BoxShadow(
                          color: colors.blue,
                          offset: Offset(0, 8.w),
                          blurRadius: 24,
                          spreadRadius: 0,
                        )
                      ]
                    ),
                    child: SvgPicture.asset('assets/bag.svg', color: colors.white, width: 24.w, height: 24.w,),
                  )
              ),
            ),
          )
        ],
      ),
    );
  }
}