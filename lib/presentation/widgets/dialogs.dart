import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void showError(BuildContext context, String e){
  showDialog(context: context, builder: (_) => AlertDialog(
    title: Text('Error'),
    content: Text(e),
    actions: [
      TextButton(onPressed: (){
        Navigator.of(context).pop();
      }, child: Text('OK'))
    ],
  ));
}