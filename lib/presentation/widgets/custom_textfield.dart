import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomField extends StatefulWidget{
  final String label;
  final String hint;
  final TextEditingController controller;
  final bool enableObscure;
  final bool isValid;
  final Function(String)? onChange;

  const CustomField({super.key, required this.label, required this.hint, required this.controller, this.enableObscure = false,
    this.onChange,
    this.isValid = true
  });

  @override
  State<CustomField> createState() => _CustomFieldState();
}

class _CustomFieldState extends State<CustomField> {
  bool isObscure = true;


  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.label,
          style: GoogleFonts.raleway(
              textStyle: TextStyle(
                  fontSize: 16,
                  color: Color(0xFF1A2530),
                  fontWeight: FontWeight.w500,
                  height: 20/16
              )
          ),
        ),
        const SizedBox(height: 12,),
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(14),
              border: Border.all(color: Colors.transparent),
              color: Color(0xFFF7F7F9)
          ),
          child: SizedBox(
            height: 48,
            child: TextField(
              obscureText: (widget.enableObscure) ? isObscure : false,
              obscuringCharacter: '•',
              controller: widget.controller,
              onChanged: widget.onChange,
              style:  GoogleFonts.raleway(
                  textStyle: TextStyle(
                      fontSize: 14,
                      height: 16/14
                  )
              ),
              decoration: InputDecoration(
                  hintText: widget.hint,
                  border: InputBorder.none,
                  hintStyle: GoogleFonts.raleway(
                      textStyle: TextStyle(
                          fontSize: 14,
                          color: Color(0xFF6A6A6A),
                          height: 16/14
                      )
                  ),
                  contentPadding: const EdgeInsets.symmetric(vertical: 10, horizontal: 14),
                  suffixIconConstraints: const BoxConstraints(minWidth: 34),
                  suffixIcon: (widget.enableObscure)?
                  GestureDetector(
                    onTap: (){

                      setState(() {
                        isObscure = !isObscure;
                      });
                    },
                    child: SvgPicture.asset(
                        'assets/eye.svg'
                      // colorFilter: ColorFilter.mode(colors.iconTint, BlendMode.color)
                    ),
                  )
                      :null

              ),
            ),
          ),
        )
      ],
    );
  }

}
