import 'package:flutter/material.dart';
import 'package:session0/presentation/pages/LogIn.dart';
import 'package:session0/presentation/widgets/custom_card.dart';
import 'package:supabase_flutter/supabase_flutter.dart';


Future<void> main() async {
  await Supabase.initialize(
    url: 'https://ndpirkfhcctoltoveofs.supabase.co',
    anonKey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Im5kcGlya2ZoY2N0b2x0b3Zlb2ZzIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTM3MTYzMDcsImV4cCI6MjAyOTI5MjMwN30.AUff60YmRHhuygJXd0vUJGWpwQafI-_2JT0pGYM50jI',
  );

  runApp(MyApp());
}


class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: LogIn(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
    );
  }
}
