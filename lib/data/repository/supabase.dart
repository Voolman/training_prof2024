import 'package:supabase_flutter/supabase_flutter.dart';

final supabase = Supabase.instance.client;

Future<Future<AuthResponse>> signIn(String email, String password) async {
  return supabase.auth.signInWithPassword(email: email, password: password);
}