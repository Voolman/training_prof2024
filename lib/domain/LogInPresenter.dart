import 'package:session0/data/repository/supabase.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> pressLogIn(String email, String password, Function onResponse, Function(String) onError) async {
  try{
    await signIn(email, password);
    onResponse();
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.toString());
  }on Exception catch(e){
    onError(e.toString());
  }
}